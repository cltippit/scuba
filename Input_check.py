def LD():
  LastDive=raw_input("Is this your last dive for the day y/n? " )
  if LastDive!='n' or 'y':
    if LastDive=='no' or LastDive=='No' or LastDive=='NO' or LastDive=='N':
      LastDive='n'
    if LastDive== 'yes' or LastDive=='Yes' or LastDive=='Y' or LastDive=='YES':
      LastDive='y'
    if LastDive!='y' or LastDive!='n':
      print("Please respond with ""y"" or ""n""")
      return LD()
    return LastDive