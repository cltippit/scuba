#Carolton Tippitt
#Mid-Term

import wx,os
from pylab import *
import numpy as np

#load the dive tables into arrays for use in program
Table1=np.loadtxt('Tippitt_midterm_divetable1.dat')
Table2=np.loadtxt('Tippitt_midterm_divetable2.dat')
Table3=np.loadtxt('Tippitt_midterm_divetable3.dat')
#list of all the group designation letters
Group=['A','B','C','D','E','F','G','H','I','J','K']
DiveProfile=[]
DiveProfile.append(['BT','DEPTH','RT','SIMIN','Letter'])
DiveLetterNum=[]
DiveLetterNum.append('Letternum')

def getLetter(row,tt):
  '''
  Takes the ith row of table 1 and the total time(TT) to find your group designation lettter.
   Compares the TT to the ith position in the row
    If TT is <= the ith number, the letter position in the Group array is returned
    If TT is > the ith number, the counters increse by one
  '''
  i=1
  letter=0
  while 1:
    if tt<=row[i]:
      return letter
    elif tt>row[i]:
      i=i+1
      letter=letter+1
    if i==11:#here the program alerts you to a decompression dive
      OutPut.AppendText('Decompression Required')
      break

def rowData1(depth,tt):
  '''
  Uses table 1 and the max depth to find the ith row and then sends the TT and the ith row to the
  getLetter function.
  '''
  i=0
  while i!=17:
    if Table1[i,0]>=depth:
      return getLetter(Table1[i,:],tt)
    i=i+1
  OutPut.AppendText('Invalid depth')

  
def rowData2(si,letternumber):
  '''
  Uses table 2 to find the new group designation letter.
  Similar procedures as rowData1 except it compares the surface interval and letter number.
  It returns a number, not a letter.  there is another function which returns the letter
  '''
  list=Table2[letternumber]
  i=int(len(list))-1
  while 1:
    #because a diver dives the deepest dive first, I start at the highest letter and work backwards
    if si>list[i]:
      i=i-1
    elif si<=list[i]:
      return i+1
    elif si<720:
      return 0
    elif i==0:
      return 0
      
def rowData3(depth,letternum):
  '''
  uses the max depth and the new letter number to find the residual nitrogen(N2) levels in your body
  Similar procedures as rowData1 function
  '''
  i=0
  while i!=13:
    if Table3[i,0]>=depth:
      return Table3[i,letternum]
    i=i+1
  OutPut.AppendText('Invalid depth')

def DiveProfileFunction(even):
  j=1
  count=0
  if Question1.GetStringSelection()=='Yes' and j==1:
    j,BT,RT,TT,DEPTH,LETTER,LETTERNUM,SIMIN=1,0,0,0,0,'A',0,0
    #finds the letter number which is the postion of the letter in the group array
    BT=int(BTInput1.GetValue())
    DEPTH=int(DepthInput1.GetValue())
    LETTERNUM=rowData1(DEPTH,BT)
    #returns the letter as a string
    LETTER=Group[int(LETTERNUM)]
    OutPut.Clear()
    OutPut.AppendText('RT=0, Group is %s'%(LETTER))
    DiveProfile.append([str(BT),str(DEPTH),str(int(RT)),str(int(SIMIN)),LETTER])
    DiveLetterNum.append(LETTERNUM)
    DepthInput1.SetValue('0')
    BTInput1.ChangeValue('0')
    SIInput1.ChangeValue('0')
    SIInput2.ChangeValue('0')
    Question1.SetSelection(1)
    j+=1
    return j
  elif Question1.GetStringSelection()=='No':
    LETTERNUM=DiveLetterNum[j]
    #get the SI in two parts hours then minutes
    SI=[int(SIInput1.GetValue()),int(SIInput2.GetValue())]
    #converts the hours to minutes then adds it to the SI(min)
    SIMIN=round(float(SI[0]*60.0+SI[1]))
    #uses the rowData2 function to find the divers new dive profile, but gives it as a number
    NEWLETTERNUM=rowData2(SIMIN,LETTERNUM)
    #this finds the acutall letter
    NEWLETTER=Group[NEWLETTERNUM]
    OutPut.Clear()
    OutPut.AppendText('New Letter is %s '%(NEWLETTER))
    
    BT=int(BTInput1.GetValue())
    DEPTH=int(DepthInput1.GetValue())
    RT=rowData3(DEPTH,NEWLETTERNUM+1)
    #finds the letter number which is the postion of the letter in the group array
    BT=int(BTInput1.GetValue())
    #calculates the total time TT as a float
    TT=RT+BT
    LETTERNUM=rowData1(DEPTH,TT)
    #returns the letter as a string
    LETTER=Group[int(LETTERNUM)]
    DiveProfile.append([str(BT),str(DEPTH),str(int(RT)),str(int(SIMIN)),LETTER])
    DiveLetterNum.append(LETTERNUM)
    OutPut.AppendText('RT=%i, TT=%i Group is %s'%(RT,TT,LETTER))
    if Question2.GetStringSelection()=='No':
      DepthInput1.SetValue('0')
      BTInput1.ChangeValue('0')
      SIInput1.ChangeValue('0')
      SIInput2.ChangeValue('0')
      j+=1
      return j
    elif Question2.GetStringSelection()=='Yes':
      np.savetxt('DiveProfile.dat',DiveProfile,fmt='%10s')
      DiveProfile.append(['BT','DEPTH','RT','SIMIN','Letter'])
      DepthInput1.SetValue('0')
      BTInput1.ChangeValue('0')
      SIInput1.ChangeValue('0')
      SIInput2.ChangeValue('0')
      Question1.SetSelection(0)
      Question2.SetSelection(1)
  else:
    OutPut.AppendText('Surface Interval?')



#Creating the GUI
app=wx.PySimpleApp()
win=wx.Frame(None,title='Simple Dive Table', size=(400,250))
win.Show()

#the set-up
Instruction1=wx.StaticText(win,-1,'Is this your first dive?',pos=(5,5))
choicelist1=['Yes','No']
Question1=wx.Choice(win,-1,pos=(250,5),choices=choicelist1)

Instruction2=wx.StaticText(win,-1,'What was your Bottom Time (BT)?',pos=(5,45))
BTInput1=wx.TextCtrl(win,pos=(250,40),size=(40,25))
Time1=wx.StaticText(win,-1,'min',pos=(290,45))

Instruction3=wx.StaticText(win,-1,'What was your Depth?',pos=(5,80))
DepthInput1=wx.TextCtrl(win,pos=(250,75),size=(40,25))
Depth1=wx.StaticText(win,-1,'ft',pos=(290,80))

Instruction4=wx.StaticText(win,-1,'What was your Surface Interval (SI)?',pos=(5,115))
SIInput1=wx.TextCtrl(win,pos=(250,110),size=(40,25))
Time2=wx.StaticText(win,-1,'hr',pos=(290,115))
SIInput2=wx.TextCtrl(win,pos=(310,110),size=(40,25))
Time3=wx.StaticText(win,-1,'min',pos=(350,115))

Instruction5=wx.StaticText(win,-1,'Is this your last dive?',pos=(5,150))
Question2=wx.Choice(win,-1,pos=(250,150),choices=choicelist1)

GetNitrogenLevel=wx.Button(win,label='Get Dive Profile',pos=(5,185))
OutPut=wx.TextCtrl(win,pos=(125,185),size=(250,25))

#Default Values
DepthInput1.SetValue('0')
BTInput1.SetValue('0')
SIInput1.SetValue('0')
SIInput2.SetValue('0')
OutPut.SetValue('Dive Profile')
Question2.SetSelection(1)

#making buttons
GetNitrogenLevel.Bind(wx.EVT_BUTTON,DiveProfileFunction)

app.MainLoop()