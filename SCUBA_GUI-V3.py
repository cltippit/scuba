#Carolton Tippitt
#Mid-Term
#Version 3

import wx
from pylab import *
from numpy import array, savetxt
from wx import App,Frame,StaticText,Choice,TextCtrl,Button,EVT_BUTTON,EVT_MENU
from wx import TE_PROCESS_ENTER,TE_PROCESS_TAB,TE_RIGHT,TE_READONLY,EVT_TEXT_ENTER
from wx import TE_MULTILINE
from os.path import join

from sys import exit

#load the dive tables into arrays for use in program
Table1=array([[10.,60.,120.,210.,300.,0.,0.,0.,0.,0.,0.,0.,0.],\
  [15.,35.,70.,110.,160.,225.,350.,0.,0.,0.,0.,0.,0.],\
    [20.,25.,50.,75.,100.,135.,180.,240.,325.,0.,0.,0.,0.],\
      [25.,20.,35.,55.,75.,100.,125.,160.,195.,245.,0.,0.,0.],\
	[30.,15.,30.,45.,60.,75.,95.,120.,145.,170.,205.,0.,0.],\
	  [35.,5.,15.,25.,40.,50.,60.,80.,100.,120.,140.,160.,0.],\
	    [40.,5.,15.,25.,30.,40.,50.,70.,80.,100.,120.,140.,160.],\
	      [50.,0.,10.,15.,25.,30.,40.,50.,60.,70.,0.,0.,0.],\
		[60.,0.,10.,15.,20.,25.,30.,40.,50.,0.,0.,0.,0.],\
		  [70.,0.,5.,10.,15.,20.,30.,35.,40.,0.,0.,0.,0.],\
		    [80.,0.,5.,10.,15.,20.,25.,30.,0.,0.,0.,0.,0.],\
		      [90.,0.,5.,10.,12.,15.,20.,25.,0.,0.,0.,0.,0.],\
			[100.,0.,5.,7.,10.,15.,20.,0.,0.,0.,0.,0.,0.],\
			  [110.,0.,0.,5.,10.,13.,15.,0.,0.,0.,0.,0.,0.],\
			    [120.,0.,0.,5.,10.,0.,0.,0.,0.,0.,0.,0.,0.],\
			      [130.,0.,0.,5.,0.,0.,0.,0.,0.,0.,0.,0.,0.]])
Table2=array([[10.,0.,0.,0.,0.,0.,0.,0.,0.,0.],\
  [201.,10.,0.,0.,0.,0.,0.,0.,0.,0.],\
    [290.,100.,10.,0.,0.,0.,0.,0.,0.,0.],\
      [349.,159.,70.,10.,0.,0.,0.,0.,0.,0.],\
	[395.,205.,118.,55.,10.,0.,0.,0.,0.,0.],\
	  [426.,238.,149.,90.,46.,10.,0.,0.,0.,0.],\
	    [456.,266.,179.,120.,76.,41.,10.,0.,0.,0.],\
	      [480.,290.,201.,144.,102.,67.,37.,10.,0.,0.],\
		[502.,313.,224.,165.,123.,90.,60.,34.,10.,0.],\
		  [531.,341.,243.,185.,141.,108.,80.,55.,32.,10.],\
		    [539.,349.,260.,202.,124.,96.,72.,50.,29.,10.]])
Table3=array([[10.,39.,88.,159.,279.,0.,0.,0.,0.,0.,0.],\
  [20.,18.,39.,62.,88.,120.,159.,208.,279.,399.,0.],\
    [30.,12.,25.,39.,54.,70.,88.,109.,132.,159.,190.],\
      [40.,7.,17.,25.,37.,49.,61.,73.,87.,101.,116.],\
	[50.,6.,13.,21.,29.,38.,47.,56.,66.,0.,0.],\
	  [60.,5.,11.,17.,24.,30.,36.,44.,0.,0.,0.],\
	    [70.,4.,9.,15.,20.,26.,31.,37.,0.,0.,0.],\
	      [80.,4.,8.,13.,18.,23.,28.,0.,0.,0.,0.],\
		[90.,3.,7.,11.,16.,20.,24.,0.,0.,0.,0.],\
		  [100.,3.,7.,10.,14.,18.,0.,0.,0.,0.,0.],\
		    [110.,3.,6.,10.,13.,0.,0.,0.,0.,0.,0.],\
		      [120.,3.,6.,9.,0.,0.,0.,0.,0.,0.,0.],\
			[130.,3.,0.,0.,0.,0.,0.,0.,0.,0.,0.]])
#list of all the group designation letters
Group=['A','B','C','D','E','F','G','H','I','J','K']
DiveProfile=[]
DiveLetterNum=[]
DiveLetterNum.append('Letternum')

def getLetter(row,tt):
  '''
  Takes the ith row of table 1 and the total time(TT) to find your group designation lettter.
   Compares the TT to the ith position in the row
    If TT is <= the ith number, the letter position in the Group array is returned
    If TT is > the ith number, the counters increse by one
  '''
  i=1
  letter=0
  while 1:
    if tt<=row[i]:
      return letter
    elif tt>row[i]:
      i=i+1
      letter=letter+1
    if i==11:#here the program alerts you to a decompression dive
      OutPut.AppendText('Decompression Required')
      break

def rowData1(depth,tt):
  '''
  Uses table 1 and the max depth to find the ith row and then sends the TT and the ith row to the
  getLetter function.
  '''
  i=0
  while i!=17:
    if Table1[i,0]>=depth:
      return getLetter(Table1[i,:],tt)
    i=i+1
  OutPut.AppendText('Invalid depth')

  
def rowData2(si,letternumber):
  '''
  Uses table 2 to find the new group designation letter.
  Similar procedures as rowData1 except it compares the surface interval and letter number.
  It returns a number, not a letter.  there is another function which returns the letter
  '''
  list=Table2[letternumber]
  i=int(len(list))-1
  while 1:
    #because a diver dives the deepest dive first, I start at the highest letter and work backwards
    if si>list[i]:
      i=i-1
    elif si<=list[i]:
      return i+1
    elif si<720:
      return 0
    elif i==0:
      return 0
      
def rowData3(depth,letternum):
  '''
  uses the max depth and the new letter number to find the residual nitrogen(N2) levels in your body
  Similar procedures as rowData1 function
  '''
  i=0
  while i!=13:
    if Table3[i,0]>=depth:
      return Table3[i,letternum]
    i=i+1
  OutPut.AppendText('Invalid depth')

class MyFrame(Frame):
	def __init__(self,parent,id,title):
		Frame.__init__(self,parent,id,title,wx.DefaultPosition,wx.Size(525,250))
		
		#the set-up
		StaticText(self,-1,'Is this your first dive?',pos=(5,5))
		choicelist1=['Yes','No']
		self.Question1=Choice(self,-1,pos=(250,5),choices=choicelist1)

		StaticText(self,-1,'What was your Bottom Time (BT)?',pos=(5,45))
		self.BTInput1=TextCtrl(self,pos=(250,40),size=(40,25),style=TE_PROCESS_ENTER|TE_PROCESS_TAB|TE_RIGHT)
		StaticText(self,-1,'min',pos=(290,45))

		StaticText(self,-1,'What was your Depth?',pos=(5,80))
		self.DepthInput1=TextCtrl(self,pos=(250,75),size=(40,25),style=TE_PROCESS_ENTER|TE_PROCESS_TAB|TE_RIGHT)
		StaticText(self,-1,'ft',pos=(290,80))

		StaticText(self,-1,'What was your Surface Interval (SI)?',pos=(5,115))
		self.SIInput1=TextCtrl(self,pos=(250,110),size=(40,25),style=TE_PROCESS_ENTER|TE_PROCESS_TAB|TE_RIGHT)
		StaticText(self,-1,'hr',pos=(290,115))
		self.SIInput2=TextCtrl(self,pos=(310,110),size=(40,25),style=TE_PROCESS_ENTER|TE_PROCESS_TAB|TE_RIGHT)
		StaticText(self,-1,'min',pos=(350,115))

		StaticText(self,-1,'Is this your last dive?',pos=(5,150))
		self.Question2=Choice(self,-1,pos=(250,150),choices=choicelist1)

		self.GetNitrogenLevel=Button(self,label='Get Dive Profile',pos=(40,185))
		self.QuitButt=Button(self,-1,label='Quit',pos=(200,185))
		
		self.OutPut=TextCtrl(self,pos=(380,5),size=(130,185),style=TE_READONLY|TE_PROCESS_TAB|TE_MULTILINE)
		
	
		#Default Values
		self.Question1.SetSelection(0)
		self.DepthInput1.SetValue('0')
		self.BTInput1.SetValue('0')
		self.SIInput1.SetValue('0')
		self.SIInput2.SetValue('0')
		self.OutPut.AppendText('Dive Profile\n')
		self.OutPut.AppendText(' BT\tDP\tRT\tSI\tGDL\n')
		self.Question2.SetSelection(1)
		
		menu_bar=wx.MenuBar()
		file_menu=wx.Menu()
		file_menu.Append(1001,'&New')
		file_menu.Append(1003,'&Save')
		file_menu.Append(1002,'&Quit')
		menu_bar.Append(file_menu,'&File')
		self.SetMenuBar(menu_bar)
		
		#making buttons
		self.GetNitrogenLevel.Bind(EVT_BUTTON,self.DiveProfileFunction)
		self.Bind(EVT_MENU,self.getQuit,id=1002)
		self.Bind(EVT_MENU,self.getNew,id=1001)
		self.Bind(EVT_MENU,self.getSave,id=1003)
		self.DepthInput1.Bind(EVT_TEXT_ENTER,self.DiveProfileFunction)
		self.SIInput1.Bind(EVT_TEXT_ENTER,self.DiveProfileFunction)
		self.SIInput2.Bind(EVT_TEXT_ENTER,self.DiveProfileFunction)
		self.QuitButt.Bind(EVT_BUTTON,self.getQuit)

	def DiveProfileFunction(self,event):
		j=1
		count=0
		if self.Question1.GetStringSelection()=='Yes':
			j,BT,RT,TT,DEPTH,LETTER,LETTERNUM,SIMIN=1,0,0,0,0,'A',0,0
			#finds the letter number which is the postion of the letter in the group array
			BT=int(self.BTInput1.GetValue())
			DEPTH=int(self.DepthInput1.GetValue())
			LETTERNUM=rowData1(DEPTH,BT)
			#returns the letter as a string
			LETTER=Group[int(LETTERNUM)]
			self.OutPut.AppendText(' %i\t%i\t%i\t%i\t%s\n'%(BT,DEPTH,RT,SIMIN,LETTER))
			DiveProfile.append([str(BT),str(DEPTH),str(int(RT)),str(int(SIMIN)),LETTER])
			DiveLetterNum.append(LETTERNUM)
			self.DepthInput1.SetValue('0')
			self.BTInput1.ChangeValue('0')
			self.SIInput1.ChangeValue('0')
			self.SIInput2.ChangeValue('0')
			self.Question1.SetSelection(1)
			j+=1
		
		elif self.Question1.GetStringSelection()=='No' and self.SIInput1.GetValue()!=0\
			and self.SIInput2.GetValue()!=0:
			LETTERNUM=DiveLetterNum[j]
			#get the SI in two parts hours then minutes
			SI=[int(self.SIInput1.GetValue()),int(self.SIInput2.GetValue())]
			#converts the hours to minutes then adds it to the SI(min)
			SIMIN=round(float(SI[0]*60.0+SI[1]))
			#uses the rowData2 function to find the divers new dive profile, but gives it as a number
			NEWLETTERNUM=rowData2(SIMIN,LETTERNUM)
			#this finds the acutall letter
			NEWLETTER=Group[NEWLETTERNUM]
			
			BT=int(self.BTInput1.GetValue())
			DEPTH=int(self.DepthInput1.GetValue())
			RT=rowData3(DEPTH,NEWLETTERNUM+1)
			#finds the letter number which is the postion of the letter in the group array
			BT=int(self.BTInput1.GetValue())
			#calculates the total time TT as a float
			TT=RT+BT
			LETTERNUM=rowData1(DEPTH,TT)
			#returns the letter as a string
			LETTER=Group[int(LETTERNUM)]
			DiveProfile.append([str(BT),str(DEPTH),str(int(RT)),str(int(SIMIN)),LETTER])
			self.OutPut.AppendText(' %i\t%i\t%i\t%i\t%s\n'%(BT,DEPTH,RT,SIMIN,LETTER))
			DiveLetterNum.append(LETTERNUM)
			#self.OutPut.AppendText('RT=%i, TT=%i Group is %s'%(RT,TT,LETTER))
			if self.Question2.GetStringSelection()=='No':
				self.DepthInput1.SetValue('0')
				self.BTInput1.ChangeValue('0')
				self.SIInput1.ChangeValue('0')
				self.SIInput2.ChangeValue('0')
				j+=1
				
			elif self.Question2.GetStringSelection()=='Yes':
				DiveProfile.append(['BT','DEPTH','RT','SIMIN','Letter'])
				self.DepthInput1.SetValue('0')
				self.BTInput1.ChangeValue('0')
				self.SIInput1.ChangeValue('0')
				self.SIInput2.ChangeValue('0')
				self.Question1.SetSelection(0)
				self.Question2.SetSelection(1)
		
		else:
			self.OutPut.AppendText('Surface Interval?\n')
	
	def getQuit(self,event):
		exit()
	
	def getNew(self,event):
		DiveProfile=[]
		DiveLetterNum=[]
		self.Question1.SetSelection(0)
		self.DepthInput1.SetValue('0')
		self.BTInput1.SetValue('0')
		self.SIInput1.SetValue('0')
		self.SIInput2.SetValue('0')
		self.OutPut.Clear()
		self.OutPut.AppendText('Dive Profile\n')
		self.OutPut.AppendText(' BT\tDP\tRT\tSI\tGDL\n')
		self.Question2.SetSelection(1)
	
	def getSave(self,event):
		dialog = wx.FileDialog(self, message='Choose a file...',defaultFile=\
			"SCUBA-Profile.txt",wildcard=".txt",style=wx.SAVE|wx.OVERWRITE_PROMPT)
		if dialog.ShowModal() == wx.ID_OK:
			saveStuff=self.OutPut.GetValue()
			# Open the file for write, write, close
			self.filename=dialog.GetFilename()
			self.dirname=dialog.GetDirectory()
			filehandle=open(join(self.dirname, self.filename),'w')
			filehandle.write(saveStuff)
			filehandle.close()
	
#Creating the GUI
class MyApp(App):
	def OnInit(self):
		frame=MyFrame(None,-1,'Dive Table GUI')
		frame.Show(True)
		self.SetTopWindow(frame)
		return True

app=MyApp()
app.MainLoop()
